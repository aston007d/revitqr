﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System.Drawing;
using System.Drawing.Imaging;

namespace RevitQRCodeImport.Classes
{
    class DirectoryHelper
    {
        private const string QR_CODES = "_QRCodes";
        private string modelPath;
        private string modelName;
        private string dirPath;
        private DirectoryInfo targetDir;

        public void CreateDir(UIApplication UIApp)
        {
            try
            {
                if (UIApp == null)
                    return;

                var activeDocument = UIApp.ActiveUIDocument.Document;
                if (activeDocument.IsWorkshared)
                {
                    modelPath = BasicFileInfo.Extract(activeDocument.PathName).CentralPath;
                    modelPath = modelPath.Replace("\\\\LINK_PASTE_HERE\\LINK_PASTE_HERE\\", "LINK_PASTE_HERE:\\");
                }
                else
                {
                    modelPath = activeDocument.PathName;
                }

                modelName = Path.GetFileNameWithoutExtension(modelPath);
                dirPath = Path.GetDirectoryName(modelPath) + $"\\{modelName}" + QR_CODES;
                targetDir = Directory.CreateDirectory(dirPath);

            }
            catch (Exception ex)
            {
                throw new Exception($"Не могу создать директорию {targetDir.FullName}: \n {ex.Message}");
            }
        }

        public string SaveImage(Bitmap bitmap, string fileName)
        {
            try
            {
                string imgPath = targetDir.FullName + "\\" + fileName + ".png";
                bitmap.Save(imgPath, ImageFormat.Png);
                return imgPath;
            }
            catch (Exception ex)
            {
                throw new Exception($"Не могу сохранить изображение: {fileName}: \n {ex.Message}");
            }
        }


    }
}
