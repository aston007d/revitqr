﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Linq;
using System.Collections.Generic;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB.Events;
using QRCoder;
using RevitQRCodeImport.Classes;
using RevitStampManager.Classes;


namespace RevitQRCodeImport
{
    [Transaction(TransactionMode.Manual)]
    [Regeneration(RegenerationOption.Manual)]
    public class QRImporter : IExternalCommand
    {
        Autodesk.Revit.DB.Document doc;
        UIApplication uiapp;
        QRCodeExporter worker;
        CustomQRBuilder builder;
        DirectoryHelper directory;

        private void InitializeComponent(ExternalCommandData cd)
        {
            this.uiapp = cd.Application;
            this.doc = uiapp.ActiveUIDocument.Document;
            this.builder = new CustomQRBuilder();
            directory = new DirectoryHelper();
            doc.Application.DocumentChanged -= new EventHandler<DocumentChangedEventArgs>(OnDocumentChanged);
            doc.Application.DocumentChanged += new EventHandler<DocumentChangedEventArgs>(OnDocumentChanged);
        }

        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            try
            {
                InitializeComponent(commandData);
                directory.CreateDir(uiapp);

                Stamp target = GS_StampManager.GetStampChecked(doc);
                System.Drawing.Bitmap qrBit = builder.GetQRCodeAsBitmap(target.BuildQuery());
                string qrPath = directory.SaveImage(qrBit, target.GetName());

                this.worker = new QRCodeExporter(doc, qrPath);

                worker.QRExport();
                worker.CreateGroup(new List<ElementId>() { worker.FamilyInstanceID, worker.ImageElementID });


                return Result.Succeeded;
            }
            catch (Exception ex)
            {
                string caption = "Ошибка";
                string content = $"Не могу завершить выполнение. Какая-то ошибка: \n {ex.Message} \n {ex.StackTrace}";
                System.Windows.Forms.MessageBox.Show(content, caption, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);

                return Result.Failed;
            }
        }

        //Собыитие срабатывает, когда пользователь ставит экземпля семейства.
        //Если поставлено то, что надо - задаются координаты центра семейства для вставки изображения
        //
        private void OnDocumentChanged( object sender, DocumentChangedEventArgs e)
        {
            var a = e.GetAddedElementIds();
            var el = doc.GetElement(a.First());

            if (!(el is FamilyInstance) ||
                (el.Name != "BaseType"))
                return;

            var fi = el as FamilyInstance;
            var cc = (fi.Location) as LocationPoint;

            var bb = fi.get_BoundingBox(doc.ActiveView);

            worker.FamilyInstanceID = fi.Id;
            worker.AnchorLocation = new XYZ(
                                        bb.Max.X - (bb.Max.X - bb.Min.X) / 2,
                                        bb.Max.Y - (bb.Max.Y - bb.Min.Y) / 2,
                                        0
                                        );

        }
    }    
}
