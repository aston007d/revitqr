﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using RevitStampManager.Classes;


namespace RevitQRCodeImport.Classes
{
    class QRCodeExporter
    {
        private XYZ anchorLocation;
        private Autodesk.Revit.DB.View currentView;
        private Document doc;
        private UIDocument uidoc;
        private readonly string path;
        private readonly string imgPath;
        private readonly string familyName;
        private Family familyRef;
        private Element element;
        private ElementId familyInstanceId;

        public ElementId FamilyInstanceID { get => familyInstanceId; set => familyInstanceId = value; }

        public ElementId ImageElementID { get => element?.Id; }

        public XYZ AnchorLocation { get => anchorLocation; set => anchorLocation = value; }

        public QRCodeExporter(Document currentDoc, string imagePath)
        {
            currentView = currentDoc.ActiveView;
            doc = currentDoc;
            uidoc = new UIDocument(doc);
            anchorLocation = new XYZ(0, 0, 0);
            path = @"LINK_PASTE_HERE";
            imgPath = imagePath;
            familyName = "QRCode";

        }

        //
        //Подгружаем семейство из файла
        //
        public void QRExport()
        {
            if (currentView != null &&
                currentView is ViewSheet
                )
            {
                familyRef = GetFamilyLoaded(familyName);
                if (familyRef == null)
                {
                    if (!LoadFamilyFromFile())
                    {
                        throw new Exception("Не могу найти файл семейства.");
                    }
                }

                PlaceFamilySymbol(familyRef);
                return;
            }

            throw new Exception("Выбранный элемент не является листом.");
        }

        private Family GetFamilyLoaded(string familyName)
        {
            FilteredElementCollector a
                = new FilteredElementCollector(doc)
                  .OfClass(typeof(Family));

            return a.FirstOrDefault<Element>(
                    e => e.Name.Equals(familyName))
                    as Family;

        }

        private bool LoadFamilyFromFile()
        {
            if (!File.Exists(path))
                return false;

            try
            {
                using (Transaction tx = new Transaction(doc))
                {
                    tx.Start("Load QRExporter");
                    doc.LoadFamily(path, out familyRef);
                    tx.Commit();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void PlaceAnImage()
        {
            if (familyInstanceId == null)
                return;

            #region Поиск и удаление ссылки на изображение
            FilteredElementCollector col = new FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_RasterImages);
 
            var img
                = from element in col
                  where element.Name == Path.GetFileName(imgPath)
                  select element;
            

            Element imgRef = img.FirstOrDefault();
            if (imgRef != null)
            {
                using (Transaction td = new Transaction(doc, "Image ref Deleting"))
                {
                    td.Start();
                    doc.Delete(imgRef.Id);
                    td.Commit();                   
                }

            }
            #endregion

            using (Transaction ts = new Transaction(doc))
            {
                Location loc;

                ts.Start("Image uploading");
                ImageImportOptions iio = new ImageImportOptions();

                doc.Import(imgPath, iio, currentView, out element);

                loc = element.Location;
                var ss = element.get_BoundingBox(currentView);
                ss.Max = anchorLocation;

                loc.Move(anchorLocation);

                Parameter parameterWidth = element.get_Parameter(BuiltInParameter.RASTER_SHEETWIDTH);
                parameterWidth.SetValueString("25.0");

                Parameter parameterLayer = element.get_Parameter(BuiltInParameter.IMPORT_BACKGROUND);
                parameterLayer.Set(0);

                ts.Commit();
            }
        }

        //Ревит требует поставить экземпляр семейства. Экземпляры могут ставиться до бесконечности
        //Есть отменить операцию - плучим исключение. Перехватываем его и запихиваем в экземпляр картинку
        //
        private void PlaceFamilySymbol(Family family)
        {
            FamilySymbol symbol = null;
            ISet<ElementId> symIds = family.GetFamilySymbolIds();
            symbol = doc.GetElement(symIds.FirstOrDefault()) as FamilySymbol;
            try
            {
                uidoc.PromptForFamilyInstancePlacement(symbol);
            }
            catch (Autodesk.Revit.Exceptions.OperationCanceledException)
            {
                PlaceAnImage();
            }
        }

        //На вход подаём имя и набор элементов (экземпляр семейства и картинку с QR кодом)
        //проверяем проект на наличие группы с таким же именем. Если есть - подтираем группу
        //и ССЫЛКУ на картинку.
        public void CreateGroup(ICollection<ElementId> elements)
        {
            if (elements.Count <= 1 || elements.Contains(null))
                return;

            string name = Path.GetFileNameWithoutExtension(imgPath) + "_Group";

            #region Поиск GroupType
            FilteredElementCollector collector
              = new FilteredElementCollector(doc)
                .OfClass((typeof(GroupType)));

            var groupTypes
              = from element in collector
                where element.Name == name
                select element;
            #endregion

            Element groupType = groupTypes.FirstOrDefault();
            if (groupType != null)
            {
                using (Transaction td = new Transaction(doc, "GroupType Deleting"))
                {
                    td.Start();
                    doc.Delete(groupType.Id);
                    td.Commit();
                }
            }

            using (Transaction t = new Transaction(doc, "Group"))
            {
                t.Start();
                var group = doc.Create.NewGroup(elements);
                group.GroupType.Name = name;
                t.Commit();
            }
        }
    }
    
}
